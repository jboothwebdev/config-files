vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.updatetime = 300
vim.opt.relativenumber = true
vim.opt.clipboard = "unnamedplus"
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 10

lvim.log.level = "warn"
lvim.colorscheme = "doom-one"

lvim.format_on_save.enabled = true

lvim.transparent_window = true

-- to disable icons and use a minimalist setup,
-- uncomment the following lvim.use_icons = false

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"

-- add your own keymapping
lvim.keys.insert_mode["jj"] = "<ESC> :w<cr>"
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.normal_mode["<leader>f"] = false
lvim.keys.normal_mode["<leader>w"] = false
lvim.keys.normal_mode["<leader>fs"] = ":w<cr>"
lvim.keys.normal_mode["<leader>sw"] = ":set wrap<cr>"
lvim.keys.normal_mode["<leader>fr"] = ":%s/"
lvim.keys.normal_mode["<leader>gs"] = ":Git<cr>"
lvim.keys.normal_mode["<leader>pe"] = ":lua vim.diagnostic.open_float(0, {scope='line'})<CR>"
lvim.keys.normal_mode["<leader>lu"] = ":LspRestart<CR>"
lvim.keys.normal_mode["<leader>w."] = "<C-w>10>"
lvim.keys.normal_mode["<leader>w,"] = "<C-w>10<"

lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"

lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["t"] = {
  name = "+Trouble",
  r = { "<cmd>Trouble lsp_references<cr>", "References" },
  f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
  d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
  q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
  l = { "<cmd>Trouble loclist<cr>", "LocationList" },
  w = { "<cmd>Trouble workspace_diagnostics<cr>", "Workspace Diagnostics" },
}

lvim.builtin.which_key.mappings["w"] = {
  name = "+windows",
  d = { "<C-w>q", "Delete window" },
  w = { ":w!<cr>", "Save file" },
  l = { "<C-w>l", "Select window right" },
  h = { "<C-w>h", "Select window right" },
  j = { "<C-w>j", "Select window down" },
  k = { "<C-w>k", "Select window up" },
  v = { ":vsplit<cr>", "Split window vertically" },
  s = { ":split<cr>", "Split horizontal" },
  q = { ":wq<cr>", "save and close" }
}

lvim.builtin.which_key.mappings["g"] = {
  s = { ":G<cr>", "Git Status" },
  p = { ":Git push<cr>", "Git push" }
}


-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "right"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false

-- if you don't want all the parsers change this to a table of the ones you want
require("lvim.lsp.manager").setup("angularls")
require("lvim.lsp.manager").setup("csharp-ls")
require("lvim.lsp.manager").setup("taplo")

lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "tsx",
  "css",
  "rust",
  "yaml",
  "vue",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enable = true
lvim.builtin.treesitter.rainbow.enable = true

-- Debugger
-- ----------
local dap = require("dap")

dap.adapters.codelldb = {
  type = "server",
  port = "${port}",
  executable = {
    command = '/Users/jeremiahbooth/.local/share/lvim/mason/bin/codelldb',
    args = { "--port", "${port}" }
  },
}

dap.configurations.rust = {
  {
    name = "Launch file",
    type = "codelldb",
    request = "attach",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
  }
}

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  {
    command = "eslint", filetypes = { "typescript", "typescriptreact", "vue", "javascript", "javascriptreact" }
  },
}


lvim.plugins = {
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  {
    "ishan9299/nvim-solarized-lua"
  },
  {
    "NTBBloodbath/doom-one.nvim"
  },
  {
    "tpope/vim-fugitive"
  },
  {
    "codota/tabnine-nvim",
    run = "./dl_binaries.sh"
  },
  { "brenoprata10/nvim-highlight-colors" },
  {
    'rest-nvim/rest.nvim',
    requres = { "nvim-lua/plenary.nvim" }
  },
  { "ray-x/lsp_signature.nvim" }
}

lvim.builtin.lualine.sections.lualine_b = { "filename" }
lvim.builtin.lualine.sections.lualine_c = { "branch" }

require("tabnine").setup({
  disable_auto_comment = true,
  accept_keymap = "<C-y>",
  dismiss_keymap = "<C-j>",
  debounce_ms = 500,
  suggestion_color = { gui = "#AABBDD", cterm = 141 },
  exclude_filetypes = { "TelescopePrompt" }
})

require('tabnine.status').status()

require("rest-nvim").setup({})
require('nvim-highlight-colors').setup {
}
