local map = vim.api.nvim_set_keymap

--basic commands
map('i', 'jj', '<ESC>', {})
map('n', '<leader>ww', ":w!<CR>", {})
map("n", '<leader>wq', ":wq<CR>", {})
map('n', '<leader>q', ":q<CR>", {})
map("n", "<leader>h",  ":noh<CR>", {})
map("n", "<leader>y", ":+y<CR>", {})

-- buffers
map('n', 'H', ":bp<CR>", {})
map('n', 'L', ":bn<CR>", {})
map('n', '<leader>c', ':bp | bd #<CR>', {})

-- windows
map('n', '<leader>wl', "<C-w>l", {})
map('n', '<leader>wj', "<C-w>j", {})
map('n', '<leader>wk', "<C-w>k", {})
map('n', '<leader>wh', "<C-w>h", {})
map('n', '<leader>wd', "<C-w>q", {})
map('n', '<leader>ws', ":split<CR>", {})
map('n', '<leader>wv', ":vsplit<CR>", {})
map('n', '<leader>w=', "<C-w>=", {})
map('n', '<leader>w.', "<C-w>10>", {})
map('n', '<leader>w,', "<C-w>10<", {})

map('n', '<leader>e', [[:NvimTreeToggle<CR>]], {})
map('n', '<leader>u', ":UndotreeToggle<CR>", {} )

--TELESCOPE
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>sf',  builtin.git_files, {})
vim.keymap.set('n', '<leader>sa', builtin.find_files, {})
vim.keymap.set('n', '<leader>st', builtin.live_grep, {})
map('n', '<leader>se', ":Telescope diagnostics<CR>", {})

-- toggles
map('n', '<leader>tl', ":IndentLinesToggle<cr>", {})
map('n', '<leader>A', ":CodeActionMenu<cr>", {})

-- GIT
map('n', '<leader>gs', ":Git<cr>", {})
map('n', '<leader>gp', ":Git push<CR>", {})
map('n', '<leader>gu', ":Git pull<CR>", {})
map('n', '<leader>gc', ':Git checkout', {})
map('n', '<leader>gb', ':Git branch', {})
map('n', '<leader>gh', ":diffget //2<CR>", {})
map('n', '<leader>gl', ":diffget //2<CR>", {})


-- PACKER
map('n', '<leader>pi', ":PackerInstall<cr>", {})
map('n', '<leader>pu', ":PackerUpdate<cr>", {})

-- LSP 
map('n', '<leader>Li', ":LspInstall", {})
map('n', '<leader>Lr', ":LspRestart<cr>", {})
map("n", "<leader>ls", ":LspInfo<cr>", {})
-- TERMINAL
map('n', '<F12>', ":FloatermToggle<CR>", {})
map('t', '<F12>', "<C-\\><C-n>:FloatermToggle<CR>", {})
map('t', '<C-e>', '<C-\\><C-n><cr>', {})

-- Debug
map('n', '<leader>dt', ":lua require('dapui').toggle()<CR>", {})
map('n', '<leader>tp', ":lua require('dap').toggle_breakpoint()<CR>", {})
map('n', '<leader>ds', ":lua require('dap').continue()<CR>", {})
map('n', '<leader>do', ":lua require('dap').step_over()<CR>", {})
map('n', '<leader>di', ":lua require('dap').step_into()<CR>", {})


