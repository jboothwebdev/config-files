vim.g.mapleader = " "
vim.g.vim_json_syntax_conceal = 0
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 4
vim.opt.signcolumn = "yes"
vim.opt.syntax = "ON"
vim.opt.spell = true
vim.opt.termguicolors = true
vim.opt.clipboard = "unnamedplus"
vim.wo.fillchars='eob: '

-- SEARCHING
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true

-- Tabs
vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 2

--THEME
vim.opt.cursorline = true
vim.opt.termguicolors = true
vim.api.nvim_command('colorscheme catppuccin-macchiato')

-- PLUGINS
require('plug')
require('keys')
require('lsp')
require('nvim-tree').setup{}
require('impatient')
require('lualine').setup{}

require'nvim-treesitter.configs'.setup{
   ensure_installed = {
        "javascript",
        "typescript",
        "vue",
        "rust",
        "html",
        "css", 
        "tsx"
    },

    auto_install = true,
    highlight = {
        enable = true,
    }
}

require("nvim-tree").setup({
  sync_root_with_cwd = true,
  view = {
    side ="right",
    signcolumn = "no"
  },
})


require('nvim-autopairs').setup({
    disable_filetype = {"TelescopePrompt", "vim"},
})

require('bufferline').setup{}

local yank_group = vim.api.nvim_create_augroup('HighlightYank', {})

vim.api.nvim_create_autocmd("TextYankPost", {
    group = yank_group,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'Search',
            timeout = 100,
        })
    end,
})

require("lspconfig").angularls.setup{}

require("transparent").setup({
})

require("lsp_signature").setup({
  bind = true,
  handler_opts = {
    border = "rounded"
  }
})

vim.keymap.set("n", "<leader>k", function() vim.lsp.buf.signature_help() end, {})

require("rest-nvim").setup({})
require('nvim-highlight-colors').setup{
}

local rt = require("rust-tools")

rt.setup({
  server = {
   on_attach = function(_, bufnr)
     vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, {buffer = bufnr})
     vim.keymap.set("n", "<leader>a", rt.code_action_group.code_action_group, {buffer = bufnr})
     vim.keymap.set("n", "<leader>gd", function() vim.lsp.buf.definition() end, opts)
     vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
     vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
     vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
     vim.keymap.set("n", "<leader>pe", function() vim.diagnostic.open_float() end, opts)
     vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
     vim.keymap.set("n", "<leader>la", function() vim.lsp.buf.code_action() end, opts)
     vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.rererences() end, opts)
     vim.keymap.set("n", "<leader>lr", function() vim.lsp.buf.rename() end, opts)
     vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
   end
  }
})

require("inlay-hints").setup()

local lsp = require('lsp-zero').preset({})

lsp.skip_server_setup({"tsserver"})

lsp.setup()

local ih = require("inlay-hints")
local lspconfig = require("lspconfig")

lspconfig.lua_ls.setup({
  on_attach = function(c, b)
    ih.on_attach(c, b)
  end,
  settings = {
    Lua = {
      hint = {
        enable = true,
      },
    },
    typescript = {
      hint = {
        enable = true,
      }
    }
  },
})

require('typescript').setup({
  server = {
    on_attach = function(client, bufnr)
      -- You can find more commands in the documentation:
      -- https://github.com/jose-elias-alvarez/typescript.nvim#commands
      vim.keymap.set("n", "<leader>gd", function() vim.lsp.buf.definition() end, opts)
      vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
      vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
      vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
      vim.keymap.set("n", "<leader>pe", function() vim.diagnostic.open_float() end, opts)
      vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
      vim.keymap.set("n", "<leader>la", function() vim.lsp.buf.code_action() end, opts)
      vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.rererences() end, opts)
      vim.keymap.set("n", "<leader>lr", function() vim.lsp.buf.rename() end, opts)
      vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
      vim.keymap.set('n', '<leader>li', '<cmd>TypescriptAddMissingImports<cr>', {buffer = bufnr})
    end
  }
})

-- Debugger
local dap = require("dap")

local mason_path = vim.fn.glob(vim.fn.stdpath "data" .. "/mason/")

dap.adapters.codelldb = {
  type = "server",
  port = "${port}",
  executable = {
    command = mason_path .. "bin/codelldb",
    args = { "--port", "${port}" }
  },
}

dap.configurations.rust = {
  {
    name = "Launch file",
    type = "codelldb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
  }
}

require("dapui").setup()

local null_ls = require("null-ls")
null_ls.setup({
  null_ls.builtins.formatting.prettier.with({
    filetypes = { "html", "json", "yaml", "markdown"}
  }),
  null_ls.builtins.formatting.eslint_d.with({
    filetypes = {"javascript", "javascriptreact", "typescript", "typescriptreact", "vue"}
  }),
  null_ls.builtins.diagnostics.rstcheck.with({
    filetypes = {"rust"}
  })

})

