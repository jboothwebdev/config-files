return require('packer').startup(function(use)
 -- Theme
    use { 'NTBBloodbath/doom-one.nvim' }
    use 'ishan9299/nvim-solarized-lua'
    use 'xiyaowong/nvim-transparent'
    use 'rebelot/kanagawa.nvim'
    use 'catppuccin/nvim'
    use 'folke/tokyonight.nvim'

    -- Utils
    --
    use 'wbthomason/packer.nvim'
    use 'lewis6991/impatient.nvim'
    use {
      'rest-nvim/rest.nvim',
      requires = {"nvim-lua/plenary.nvim"}
   }
   -- Color
   use "brenoprata10/nvim-highlight-colors"

    -- Files
    use {
        'kyazdani42/nvim-tree.lua',
        requires = 'kyazdani42/nvim-web-devicons'
    }
    use {
        'nvim-lualine/lualine.nvim',                     -- statusline
        requires = {'kyazdani42/nvim-web-devicons',
        opt = true}
      }
    use {
        'nvim-telescope/telescope.nvim',                 -- fuzzy finder
        requires = { {'nvim-lua/plenary.nvim'} }
    }
    use { 'tpope/vim-fugitive' }                       -- git integration
    use { 'junegunn/gv.vim' }                          -- commit history
    use 'tpope/vim-commentary'

    --Terminal
    use 'voldikss/vim-floaterm'

    -- Treestters
    --
    use {
        'nvim-treesitter/nvim-treesitter'
    }
    -- UNDO TREE
    use 'mbbill/undotree'

    -- LSP
    use {
      'VonHeikemen/lsp-zero.nvim',
      branch = 'v1.x',
      requires = {
        -- LSP Support
        {'neovim/nvim-lspconfig'},
        {'williamboman/mason.nvim'},
        {'williamboman/mason-lspconfig.nvim'},

        -- Autocompletion
        {'hrsh7th/nvim-cmp'},
        {'hrsh7th/cmp-buffer'},
        {'hrsh7th/cmp-path'},
        {'saadparwaiz1/cmp_luasnip'},
        {'hrsh7th/cmp-nvim-lsp'},
        {'hrsh7th/cmp-nvim-lua'},

        -- Snippets
        {'L3MON4D3/LuaSnip'},
        {'rafamadriz/friendly-snippets'},
      }
    }
    use {'jose-elias-alvarez/null-ls.nvim', 
  requires = {'nvim-lua/plenary.nvim'} }
  use("folke/zen-mode.nvim")
  use {
	"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
  }
  use {'akinsho/bufferline.nvim', tag = "v3.*", requires = 'nvim-tree/nvim-web-devicons'}
  use({
    "kylechui/nvim-surround",
    tag = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
        require("nvim-surround").setup({
            -- Configuration here, or leave empty to use defaults
        })
    end
})
use 'mfussenegger/nvim-dap'
use {'rcarriga/nvim-dap-ui', requires = {"mfussenegger/nvim-dap"}}
use 'simrat39/rust-tools.nvim'
use 'simrat39/inlay-hints.nvim'
use {
  'rust-lang/rust.vim',
}
use 'jose-elias-alvarez/typescript.nvim'
use {"zbirenbaum/copilot.lua"}
use {
    'saecki/crates.nvim',
    tag = 'v0.3.0',
    requires = { 'nvim-lua/plenary.nvim' },
    config = function()
        require('crates').setup()
    end,
  }
end)

